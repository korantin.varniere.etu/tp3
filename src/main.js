import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];


const linkLaCarte = document.querySelector('a[href="/"]');
linkLaCarte.classList += ' active';


pizzaList.pizzas = data; // appel du setter

/* B 2.1.2 */
const logo = document.querySelector('.logo');
logo.innerHTML += "<small>Les pizzas c'est la vie</small>";

/* B 2.2.2 
const linkLaCarte = document.querySelector('a[href="/"]');
linkLaCarte.classList += ' active';*/

/* C 2.2 */
const newsContainer = document.querySelector('.newsContainer');
newsContainer.setAttribute('style', 'display: block;');

/* C 2.3 */
const closeButton = document.querySelector('.closeButton');
closeButton.addEventListener('click', () => {
    newsContainer.style = "display: none;";
});

/* C 3.1 */
import Component from './components/Component.js';
import PizzaForm from './pages/PizzaForm.js';
const aboutPage = new Component('section', null, 'Ce site est génial'), pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

/* C 3.2 */
Router.menuElement = document.querySelector('.mainMenu');

/* E 2 */
window.onpopstate = event => {
    Router.navigate(document.location.pathname, false);
};

Router.navigate(document.location.pathname); // affiche la liste des pizzas